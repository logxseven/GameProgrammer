# GameProgrammer E-Book

E-books collected according to Milo Yip's learning path （ [Link](https://github.com/miloyip/game-programmer) ）



## 01_Computer Science

### Algorithm

> - [x] [Algorithms 4th Edition](https://github.com/kurong00/GameProgrammer/blob/master/01_Computer%20Science/Algorithm/Algorithhms%204th%20Edition%20by%20Robert%20Sedgewick%2C%20Kevin%20Wayne.pdf)
> - [x] [Introduction to Algorithms](https://github.com/kurong00/GameProgrammer/blob/master/01_Computer%20Science/Algorithm/Introduction%20to%20Algorithms.pdf)

### CS Foundation

> - [x] [Computer Systems](https://github.com/kurong00/GameProgrammer/blob/master/01_Computer%20Science/CS%20Foundation/Computer%20Systems.pdf)
> - [x] [Modern Operating Systems (4th edition)](https://github.com/kurong00/GameProgrammer/blob/master/01_Computer%20Science/CS%20Foundation/Modern%20Operating%20Systems%20(4th%20edition).pdf)
-------------------------------

## 02_Programming Language

### C++
> - [x] [C++ Primer, 5th Edition](https://github.com/kurong00/GameProgrammer/blob/master/02_Programming%20Language/C%2B%2B/C%2B%2B%20Primer%2C%205th%20Edition.pdf)
> - [x] [Effective C++](https://github.com/kurong00/GameProgrammer/blob/master/02_Programming%20Language/C%2B%2B/Effective%20C%2B%2B.pdf)
> - [x] [Effective Modern C++ 2014](https://github.com/kurong00/GameProgrammer/blob/master/02_Programming%20Language/C%2B%2B/Effective%20Modern%20C%2B%2B%202014.pdf)
> - [x] [C++ Coding Standards](https://github.com/kurong00/GameProgrammer/blob/master/02_Programming%20Language/C%2B%2B/C%2B%2B%20Coding%20Standards.pdf)

### C#
> - [ ] New book: Microsoft Visual C# Step by Step, Eighth Edition
> - [x] [C# in Depth, Third Edition](https://github.com/kurong00/GameProgrammer/blob/master/02_Programming%20Language/C%23/C%23%20in%20Depth%2C%20Third%20Edition.pdf)
> - [ ] CLR via C#

### Lua
> - [x] [Programming in Lua 4th Edition](https://github.com/kurong00/GameProgrammer/blob/master/02_Programming%20Language/Lua/Programming%20in%20Lua%204th%20Edition.pdf)
> - [ ] Lua Programming Gems
-----------------------------------

## 03_Mathematics Game Programming

### Begining Mathematics for Game Programming
> - [x] [Mathematics for 3D Game Programming and Computer Graphics, 3rd Edition, 2011](https://github.com/kurong00/GameProgrammer/blob/master/03_Mathematics%20Game%20Programming/Beginning%20Mathematics%20for%20Game%20Programming/Mathematics%20for%203D%20Game%20Programming%20and%20Computer%20Graphics%2C%203rd%20Edition%2C%202011.pdf)
> - [x] [3D游戏与计算机图形学中的数学方法](https://github.com/kurong00/GameProgrammer/blob/master/03_Mathematics%20Game%20Programming/Beginning%20Mathematics%20for%20Game%20Programming/3D%E6%B8%B8%E6%88%8F%E4%B8%8E%E8%AE%A1%E7%AE%97%E6%9C%BA%E5%9B%BE%E5%BD%A2%E5%AD%A6%E4%B8%AD%E7%9A%84%E6%95%B0%E5%AD%A6%E6%96%B9%E6%B3%95.pdf)
> - [x] [Foundations of Game Engine Development, Volume 1 Mathematics](https://github.com/kurong00/GameProgrammer/blob/master/03_Mathematics%20Game%20Programming/Beginning%20Mathematics%20for%20Game%20Programming/Foundations%20of%20Game%20Engine%20Development%2C%20Volume%201%20Mathematics.pdf)

### Advanced Mathematics for 3D Game Programming
> - [ ] Geometric Tools for Computer Graphics
> - [x] [计算机图形学几何工具算法详解](https://github.com/kurong00/GameProgrammer/blob/master/03_Mathematics%20Game%20Programming/Advanced%20Mathematics%20for%20Game%20Programming/%E8%AE%A1%E7%AE%97%E6%9C%BA%E5%9B%BE%E5%BD%A2%E5%AD%A6%E5%87%A0%E4%BD%95%E5%B7%A5%E5%85%B7%E7%AE%97%E6%B3%95%E8%AF%A6%E8%A7%A3.PDF)
-----------------------------------

## 04_Game Programming

### Beginning Game Programming

#### From Unity
> - [x] [Introduction to Game Design, Prototyping, and Development](https://github.com/kurong00/GameProgrammer/blob/master/04_Game%20Programming/Beginning%20Game%20Programming/From%20Unity/Introduction%20to%20Game%20Design%2C%20Prototyping%2C%20and%20Development.pdf)
> - [x] [Unity In Action](https://github.com/kurong00/GameProgrammer/blob/master/04_Game%20Programming/Beginning%20Game%20Programming/From%20Unity/Unity%20In%20Action.pdf)

### Intermediate Game Programming
> - [x] [Game Programming Algorithms and Techniques](https://github.com/kurong00/GameProgrammer/blob/master/04_Game%20Programming/Intermediate%20Game%20Programming/Game%20Programming%20Algorithms%20and%20Techniques.pdf)
> - [x] [Game Programming Patterns](https://github.com/kurong00/GameProgrammer/blob/master/04_Game%20Programming/Intermediate%20Game%20Programming/Game%20Programming%20Patterns.pdf)
> - [ ] Game Programming Golden Rules
-----------------------------------

## 05_Game Engine Development
### Beginning Game Engine Development
> - [x] [Game Engine Architecture](https://github.com/kurong00/GameProgrammer/blob/master/05_Game%20Engine%20Development/Game%20Engine%20Architecture.pdf)

### Optimization
> - [ ] Video Game Optimization
> - [x] [Unity 5 Game Optimization](https://github.com/kurong00/GameProgrammer/blob/master/05_Game%20Engine%20Development/Optimization/Unity%205%20Game%20Optimization.pdf)
-------------
## 06_Computer Graphics
### Beginning CG Programming
> - [x] [OpenGL Programming Guide 9th Edition](https://github.com/kurong00/GameProgrammer/blob/master/06_Computer%20Graphics/Beginning%20CG%20Programming/OpenGL%20Programming%20Guide%209th%20Edition.pdf)
> - [ ] Real-Time 3D Rendering with DirectX and HLSL pdf
### Beginning CG Theory
> - [x] [Fundamentals of Computer Graphics, Fourth Edition](https://github.com/kurong00/GameProgrammer/blob/master/06_Computer%20Graphics/Begin%20CG%20Theory/Fundamentals%20of%20Computer%20Graphics%2C%20Fourth%20Edition.pdf)

### Advanced CG
> - [x] [Real Time Rendering](https://github.com/kurong00/GameProgrammer/blob/master/06_Computer%20Graphics/Advanced%20CG/Real%20Time%20Rendering.pdf)

---------
## 07_Game Artificial Intelligence
### Beginning Game AI
> - [x] [Artificial Intelligence for Games](https://github.com/kurong00/GameProgrammer/blob/master/07_Game%20Artificial%20Intelligence/Artificial%20Intelligence%20for%20Games.pdf)
> - [X] [Unity AI Game Programming 2nd](https://github.com/kurong00/GameProgrammer/blob/master/07_Game%20Artificial%20Intelligence/Unity%20AI%20Game%20Programming%20-%20Second%20Edition.pdf)
### Intermediate Game AI
> - [x] [Artificial Intelligence A Modern Approach (3rd Edition)](https://github.com/kurong00/GameProgrammer/blob/master/07_Game%20Artificial%20Intelligence/Artificial%20Intelligence%20A%20Modern%20Approach%20(3rd%20Edition).pdf)

------------
  


![](https://github.com/kurong00/GameProgrammer/blob/master/game-programmer.jpg?raw=true)